## Synopsis

Adaptation du jeu Extreme TuxRacer � l'aide de Unity, dans le cadre de l'UE LIF Projet � l'Universit� Lyon 1 Claude Bernard, en utilisant les anciennes ressources.

## UnityEditor

Pour lancer le projet dans l'Editor de Unity :
- Cr�er un nouveau projet sous Unity
- Importer le custom package TuxAssets pr�sent dans le dossier AssetsUnity
- Copier coller le dossier ProjectSettings dans le projet Unity cr��
- Glisser toutes les sc�nes dans la "Hierarchy" pr�sentes dans le dossier Assets/Scenes
- Unload toutes les sc�nes, sauf "Menu"

## UnityApplication

Dans le dossier "Application Windows" se trouve l'ex�cutable Windows avec les ressources n�cessaires.

## Objectifs

L'objectif principal est r�alis� : TUnityRacer est un jeu "complet" (vingtaine de courses), test� sous Windows, Mac, et iPhone.

Le jeu tourne, les meilleurs temps sont enregistr�s, mais Tux est tout de m�me quelques fois bug�.

## Participants

Cl�ment Lemeunier, Vincent Rossero, Ma�lle Pinto
